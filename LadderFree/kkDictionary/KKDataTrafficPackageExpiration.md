###### 玩转金刚梯>金刚字典>
### 金刚流量包有效期

- 所谓<strong> 金刚流量包有效期 </strong>是指一段特定时间区间
  - 在该时间区间内，[ 流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackage.md)内[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)是有效、可被使用的
  - 早于或晚于该时间区间，即使[ 流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackage.md)内有[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)，但[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)无效、不可被使用的
- [ 金刚号梯 ](/LadderFree/kkDictionary/KKLadder.md)的<strong> 流量包有效期 </strong >共有以下几种：
   - [ 免费流量包 ](/LadderFree/kkDictionary)有效期：
     - 天
     - 月
   - [ 收费流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackageToll.md)有效期：
     - 月
     - 半年
     - 年

-  [ 金刚App梯 ](/LadderFree/kkDictionary/KKLadderAPP.md)的<strong> 流量包有效期 </strong >待定

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

