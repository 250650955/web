
###### 玩转金刚梯>金刚字典>
### 积分
- <strong> 积分 </strong>（即<strong> 金刚积分 </strong>）是：
  - [ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)发行的、当前仅可流通于[ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)与[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)之间的电子代金券

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
