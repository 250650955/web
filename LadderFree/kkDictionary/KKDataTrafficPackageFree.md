###### 玩转金刚梯>金刚字典>

- 所谓<strong> 金刚免费流量包 </strong >是指：
  - [ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)向 [ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)赠送的、[ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)不必花费
    - [ 积分 ](/LadderFree/kkDictionary/KKPoints.md)或
    - 金钱 购买
  - 即可获得的[ 流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackage.md)

- 据现行政策，[金刚公司 ](/list_kk.md)提供以下<font color="Red"> 免费流量 </font>：
  - [ 普通金刚号 ](/singlepurposekkid.md)+Windows+[ SSL客户端 ](/getSSLclientapp.md) 一一 无上限供应<font color="Red"> 免费流量 </font>
  - [ 万能金刚号 ](/multipurposekkid.md) 一一 每天供应[ 小额 ](/smallamountkkdatatraffic.md)<font color="Red"> 免费流量 </font>，次日清零再供
    - 供应前提
      - 当其上捆绑的[ 大宗流量 ](/bulkkkdatatraffic.md)包[ 过期 ](/kkdatatrafficexpired.md)时
      - 当其上捆绑的[ 大宗流量 ](/bulkkkdatatraffic.md)包包内[ 流量提前耗尽 ](/kkdatatrafficisexhaustedearly.md)时
      - 当其上捆绑的[ 小额 ](/smallamountkkdatatraffic.md)<font color="Red"> 免费流量 </font>包被清零时
    - 供应时刻
      - 北京时间每日3:00开始为[ 万能金刚号 ](/multipurposekkid.md)充[ 小额 ](/smallamountkkdatatraffic.md)<font color="Red"> 免费流量 </font>
  - 新注册[ 金刚用户 ](/kkuser.md)的第1个[ 万能金刚号 ](/multipurposekkid.md) 一一 一次性供30GB<font color="Red"> 免费流量 </font>，有效期1个月


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)




