###### 金刚梯>金刚帮助>

### 金刚1.0金刚号梯
- [概览](/kkproducts1.0.md)
- [产品获取与配置](/list_kkproducts1.0.md)
- [术语 金刚中文网类](/list_helpkkvpn1.0.md)
  - [金刚网](/kksitecn.md)
  - [金刚用户](/kkuser.md)
  - [金刚用户与金刚网的关系](/mappingrelationshipbetweenkkuser&kksitecn.md)
  - [金刚账户](/kkaccount.md)
  - [登录金刚网的用户名](/kkusername&passwdonkksitecn.md)
  - [登录金刚网的密码](/kkusername&passwdonkksitecn.md)
  - [登录金刚网所需的初始密码](/initialpasswdforloginkksitecn.md)

- [术语 金刚号类](/)
  - [金刚号](/kkid.md)
  - [金刚号的长相](/kkidform.md)
  - [金刚号与设备的关系](/mappingrelationshipbetweenkkid&device.md)
  - [金刚号与流量的关系](/mappingrelationshipbetweenkkid&kkdatatraffic.md)
  - [万能金刚号](/multipurposekkid.md)
  - [普通金刚号](/singlepurposekkid.md)
  - [金刚号的配套参数及配置方法](/parametersofkkid.md)
  - [金刚号的有效期](/kkidvalidity.md)
  - [主号](/mainkkid.md) 
  - [副号](/auxiliarykkid.md)
  - [主号、副号的用途](/useofkkid.md) 
  - [<font color="Red"> 一拖九 </font>](/onefornine.md)
  - [金刚号的价格](/kkidprice.md)
- [术语 流量类]()
  - [金刚流量](/kkdatatraffic.md)
  - [金刚流量包](/kkdatatrafficpackage.md)
  - [金刚免费流量](/kkdatatrafficfree.md)
  - [流量包尺寸](/kkdatatrafficsize.md)
  - [大宗流量](/bulkkkdatatraffic.md)
  - [小额流量](/smallamountkkdatatraffic.md)
  - [流量包有效期](/kkdatatrafficvalidityperiod.md)
  - [流量包有效期的起算时刻](/kkdatatrafficpakagevalidityperiodstarttime.md)
  - [流量提前耗尽](/kkdatatrafficisexhaustedearly.md)
  - [流量过期](/kkdatatrafficexpired.md)
  - [线下购买](/offlinepurchasedatatraffic.md)

- [术语 积分类]()
  - [积分](/kkpoints.md)
  - [积分用途](/useofkkpoints.md)
  - [积分与美元的兑换比例](/kkpointstoexchangedollars.md)
  - [可用积分购买的商品](/kkgoodsthatcanbepurchasedwithkkpoints.md)
  - [获取积分的方式](/waystoearnkkpoints.md)
  - [快速赚取积分的方式](/toearnpointsquickly.md)
  - [推荐朋友赚积分如何操作？](/workingmethodsofkkreferee.md)
  - [如何用积分为我的金刚号购买流量？](/thewaytobuydatatrafficwithpoints.md)
  - [积分有有效期](/kkpointsexpired.md)
  - [积分交易所](/kkpointexchange.md)


- [操作 金刚网类](/)
  - [忘记了登录金刚网的密码，我该怎么办？](/forgettenpasswdonkksite.md)
  - [忘记了在金刚网的注册邮箱，我该怎么办？](/forgettenregemailaddress.md)
  - [如何在金刚网注册？](/reginkksitecn.md)
  - [可在金刚网注册多个金刚账户吗？](/mutimailboxreginkksitecn.md)
  - [可用短命邮箱注册吗？](/disposableemailreg.md)
  - [如何改变我在金刚网的登录密码？]()
  - [我想把我在金刚网的注册邮箱改变为另一个邮箱，可以吗？]()

- [操作 金刚号类](/)
    - [如何获得金刚号？](/getkkid.md)
    - [如何获得副号？](/getauxiliarykkid.md)
    - [如何获得c9开头的金刚号？](//getkkidstartingwithc9.md)
    - [金刚号如何配置？](/list_kkproducts1.0.md)
    - [配置金刚号时应注意哪些事项？](/configurationconsiderations.md)
    - [我的金刚号无法连通，我该怎么办？](/)
    - [如何找回金刚号并配入新手机？](/changetoanewphone.md)
    - [如何查询我有几个金刚号？](/howmanykkiddoihave.md)
    - [如何找回金刚号的配套参数？](/getbackparameters.md)
    - [金刚密码已泄露，如何改密？](/changekkidpasswd.md)
    - [如何放弃某个金刚号？](/kkiddrop.md)
    - [我可以持有多个金刚号吗？](/mappingrelationshipbetweenkkid&kkuser.md)
    - [如何知道我名下有几个金刚号？](/howmanykkiddoihave.md)
    - [如何知道我的金刚号的剩余流量？](/howmanykkiddoihave.md)
    - [我能把同一金刚号配入手机和iPad上同时使用吗？](/onefornine.md)
    - [如何获取普通金刚号（c9开头）？](/getkkidstartingwithc9.md)
    - [如何获取与普通金刚号（c9开头）配套使用的SSL型客户端？](/getSSLclientapp.md)


- [操作 流量类]()
    - [我刚买完流量，为何又收到续费提醒邮件？](/刚买流量又被提醒续费)
    - [如何为我的金刚号购买下一期流量？]()
    - [我想改按月为按年购买流量，该怎么操作？]()
    - [我想改按年为按月购买流量，该怎么操作？]()
    - [我的流量包尺寸是每月30GB，想改为每月90GB，该怎么操作？]()
    - [我的流量包尺寸是每月90GB，想改为每月30GB，该怎么操作？]()
    - [如何查询我的金刚号的剩余流量？](/howmanykkiddoihave.md)
    - [如何知道流量提前耗尽了？](/流量提前耗尽的识别)
    - [如何知道流量提前耗尽了？](/kkdatatrafficisexhaustedearlyidentify.md)
    - [如何知道流量过期了？](/kkdatatrafficexpiredidentify.md)
    - [如何查询我的某个金刚号还是剩多少流量？](/howmanykkiddoihave.md)
    - [流量提前耗尽后我该怎么办？](/)
    - [过期流量包包内剩余流量还可以用吗？](/流量包过期后剩余流量还可以用吗)
    - [流量过期后我该怎么办？](/)
    - [我想用积分买流量，该如何操作？](/thewaytobuydatatrafficwithpoints.md)
    - [为何一定要区分<font color="Red"> 流量提前耗尽 </font>与<font color="Red"> 流量过期 </font>？](/reasonsfordistinguishingbetweenkkdatatrafficexpiration&earlyexhaustion.md)
    - [哪些情景属于 即买即用？](/哪些情景属于即买即用)
    - [哪些情景属于 预购下一期流量？](/哪些情景属于预购下一期流量)

- [操作 积分类](/)
    - [如何查询我有多少积分？]()
    - [有人扫我散发的二维码并用短命邮箱注册成功，他注册成功我所赚积分仍然有效，对吗？](/短命邮箱注册之奖励积分)
    - [金刚是如何回购积分的？](/buybackpoints.md)

#### 推荐阅读

- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [金刚2.0app梯](/list_helpkkvpn2.0.md)
