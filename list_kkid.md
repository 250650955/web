###### 金刚梯>金刚帮助>金刚公司>金刚1.0金刚号梯>
### 术语 金刚号类

  - [金刚号](/kkid.md)
  - [金刚号的长相](/kkidform.md)
  - [主号](/mainkkid.md) 
  - [副号](/auxiliarykkid.md)
  - [主号、副号的用途](/useofkkid.md) 
  - [万能金刚号](/multipurposekkid.md)
  - [普通金刚号](/singlepurposekkid.md)
  - [金刚号与设备的关系](/mappingrelationshipbetweenkkid&device.md)
  - [金刚号与流量的关系](/mappingrelationshipbetweenkkid&kkdatatraffic.md)
  - [金刚号的配套参数及配置方法](/parametersofkkid.md)
  - [金刚号的有效期](/kkidvalidity.md)
  - [<font color="Red"> 一拖九 </font>](/onefornine.md)
  - [金刚号的价格](/kkidprice.md)


#### 推荐阅读

- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚公司](/list_kk.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
