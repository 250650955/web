###### 金刚梯>金刚帮助>金刚公司类>金刚中文网类>
### 问：什么是重置密码？

答：<font color="Red"> 重置密码 </font>即变更登录[ 金刚网 ](/kksitecn.md)的密码
- 当<font color="Red"> 重置密码 </font>时，不必先登录[ 金刚网 ](/kksitecn.md)，在未登录状态下即可<font color="Red"> 重置密码 </font>
- 当<font color="Red"> 重置密码 </font>时，不必录入原密码
- 忘记原密码后，只要记得[ 注册邮箱 ](/emailaddressforregonkksitecn.md)，即可通过<font color="Red"> 重置密码 </font>登录[ 金刚网 ](/kksitecn.md)
- 若需立即<font color="Red"> 重置密码 </font>，请[ 连通 ](/useofkkid.md)[ 金刚号 ](/kkid.md)，[点击此处](https://www.atozitpro.net/zh/password-reset/)

#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚公司类](/list_kk.md)
- [金刚中文网类](/list_kksitecn.md)
